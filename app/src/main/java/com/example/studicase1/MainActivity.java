package com.example.studicase1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private TextView hasil;
    private EditText editText1, editText2;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);
        hasil = (TextView) findViewById(R.id.hasil);
    }

    public void count(View view) {
        String alas = editText1.getText().toString();
        String tinggi = editText2.getText().toString();

        double a = Double.parseDouble(alas);
        double t = Double.parseDouble(tinggi);
        double luas = a * t;
        hasil.setText(" "+luas);

    }
}
