package com.example.studicase2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class CheckoutActivity extends AppCompatActivity {
    TextView textTujuan, textTanggalKeberangkatan, textWaktuKeberangkatan, textTanggalPulang, textWaktuPulang, HargaTotal, textJumlahTiket;
    Button konfirmasi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        textTujuan = (TextView)findViewById(R.id.textTujuan);
        textTanggalKeberangkatan = (TextView)findViewById(R.id.textTanggalKeberangkatan);
        textWaktuKeberangkatan = (TextView)findViewById(R.id.textWaktuKeberangkatan);
        textTanggalPulang = (TextView)findViewById(R.id.textTanggalPulang);
        textWaktuPulang = (TextView)findViewById(R.id.textWaktuPulang);
        textJumlahTiket = (TextView)findViewById(R.id.textJumlahTiket);
        HargaTotal = (TextView)findViewById(R.id.hargaTotal);

        Intent in = getIntent();

        String tujuan = in.getStringExtra("daerahTujuan");
        String tanggalBerangkat = in.getStringExtra("tanggalBerangkat");
        String waktuBerangkat = in.getStringExtra("waktuBerangkat");
        String tanggalPulang = in.getStringExtra("tanggalPulang");
        String waktuPulang = in.getStringExtra("waktuPulang");
        String jumlahTiket = in.getStringExtra("jumlahTiket");
        String hargaTotal = in.getStringExtra("hargaTotal");

        textTujuan.setText(tujuan);
        textTanggalKeberangkatan.setText(tanggalBerangkat);
        textWaktuKeberangkatan.setText(waktuBerangkat);
        textTanggalPulang.setText(tanggalPulang);
        textWaktuPulang.setText(waktuPulang);
        textJumlahTiket.setText(jumlahTiket);
        HargaTotal.setText(hargaTotal);


        konfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Selamat Menikmati Perjalanan Anda...!!!", Toast.LENGTH_LONG).show();
                finish();
                moveTaskToBack(true);
            }
        });

    }
}
