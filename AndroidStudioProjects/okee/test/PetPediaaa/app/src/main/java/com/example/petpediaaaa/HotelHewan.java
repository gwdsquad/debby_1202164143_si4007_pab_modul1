package com.example.petpediaaaa;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * A simple {@link Fragment} subclass.
 */
public class HotelHewan extends Fragment {
    FirebaseAuth mAuth;

    public HotelHewan() {
        // Required empty public constructor
    }

    EditText namapel, nohp, checkin, checkout, hewan, ras, jumlah;
    Button reserv;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_hotel_hewan, container, false);
        namapel = view.findViewById(R.id.namapel);
        nohp = view.findViewById(R.id.nohp);
        checkin = view.findViewById(R.id.editText_checkin);
        checkout = view.findViewById(R.id.editText_checkout);
        hewan = view.findViewById(R.id.editText_hewan);
        ras = view.findViewById(R.id.editText_ras);
        jumlah = view.findViewById(R.id.editText_jumlah);

        mAuth = FirebaseAuth.getInstance();

        reserv = view.findViewById(R.id.lakukan_reservasi);
        reserv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAuth.getCurrentUser() == null) {
                    startActivity(new Intent(view.getContext(), SignInActivity.class));
                    return;
                }

                if (namapel.getText().toString().equals("")) {
                    namapel.setError("Lengkapi data berikut");
                }else if (nohp.getText().toString().equals("")) {
                    nohp.setError("Lengkapi data berikut");
                } else if (checkin.getText().toString().equals("")) {
                    checkin.setError("Lengkapi data berikut");
                }else if (checkout.getText().toString().equals("")) {
                    checkout.setError("Lengkapi data berikut");
                } else if (hewan.getText().toString().equals("")) {
                    hewan.setError("Lengkapi data berikut");
                } else if (ras.getText().toString().equals("")) {
                    ras.setError("Lengkapi data berikut");
                } else if (jumlah.getText().toString().equals("")) {
                    jumlah.setError("Lengkapi data berikut");
                } else {
                    ProgressDialog progressDialog = new ProgressDialog(getContext());
                    progressDialog.setMessage("Melakukan Reservasi");

                    FirebaseAuth mAuth = FirebaseAuth.getInstance();
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                            .child("reservasi")
                            .child(mAuth.getCurrentUser().getUid());
                    String key = ref.push().getKey();

                    ref = ref.child(key);
                    ref.child("nama").setValue(namapel.getText().toString());
                    ref.child("nohp").setValue(nohp.getText().toString());
                    ref.child("hewan").setValue(hewan.getText().toString());
                    ref.child("checkin").setValue(checkin.getText().toString());
                    ref.child("checkout").setValue(checkout.getText().toString());
                    ref.child("ras").setValue(ras.getText().toString());
                    ref.child("jumlah").setValue(jumlah.getText().toString());
                    Toast.makeText(view.getContext(), "Reservasi berhasil", Toast.LENGTH_SHORT).show();
                    namapel.setText("");
                    nohp.setText("");
                    checkin.setText("");
                    checkout.setText("");
                    hewan.setText("");
                    ras.setText("");
                    jumlah.setText("");
                    progressDialog.dismiss();
                }
            }
        });

        return view;
    }

}
