package com.example.petpediaaaa;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment {
public static Switch dark,big;
EditText email,displayName;
Button save;
CardView logout;
FirebaseAuth mAth;
    SharedPreferences sharedPreferences;

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        sharedPreferences = getActivity().getSharedPreferences("settings", Context.MODE_PRIVATE);

        mAth = FirebaseAuth.getInstance();

        //UI
        dark = view.findViewById(R.id.swDark);
        big = view.findViewById(R.id.swBig);

        dark.setChecked(sharedPreferences.getBoolean("darkMode",false));
        big.setChecked(sharedPreferences.getBoolean("bigFont",false));

        dark.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("darkMode",isChecked);
                editor.apply();
                aturTema(isChecked);
                getActivity().recreate();

            }
        });

        big.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("bigFont",isChecked);
            }
        });



        //account
        email = view.findViewById(R.id.edEmail);
        displayName = view.findViewById(R.id.edName);
        save = view.findViewById(R.id.btnSave);
        if (mAth.getCurrentUser()!=null){
            email.setText(mAth.getCurrentUser().getEmail());
            displayName.setText(mAth.getCurrentUser().getDisplayName());
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!email.getText().toString().equals(mAth.getCurrentUser().getEmail()) && !email.getText().toString().equals("")){
                    mAth.getCurrentUser().updateEmail(email.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
                                        Toast.makeText(getContext(), "Email updated", Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(getContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
                if (!displayName.getText().toString().equals(mAth.getCurrentUser().getDisplayName()) && !displayName.getText().toString().equals("")){
                    FirebaseUser user = mAth.getCurrentUser();
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(displayName.getText().toString()).build();

                    user.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                Toast.makeText(getContext(), "Name updated!", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

            }
        });

        //logout
        logout = view.findViewById(R.id.logout);
        if (mAth.getCurrentUser()==null){
            logout.setVisibility(View.GONE);
        }else{
            logout.setVisibility(View.VISIBLE);
            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAth.signOut();
                    Toast.makeText(getContext(), "Log Out Succesfull", Toast.LENGTH_SHORT).show();
                    getActivity().recreate();
                }
            });
        }





        return view;
    }

    void aturTema(boolean dark){
        if (!dark){
            /*getActivity().setTheme(R.style.AppTheme);*/
            getActivity().setTheme(R.style.lightBar);
        }else{
          /*  getActivity().setTheme(R.style.DarkTheme);*/
            getActivity().setTheme(R.style.darkBar);

        }
    }

}
