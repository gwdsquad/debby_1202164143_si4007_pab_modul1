package com.example.petpediaaaa;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

public class DetailActivity extends AppCompatActivity {
TextView judul,isi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        judul = findViewById(R.id.newsTitleDetail);
        isi = findViewById(R.id.subTitleDetail);

        try {
            judul.setText(getIntent().getStringExtra("judul"));
            isi.setText(getIntent().getStringExtra("isi"));
        }catch (Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            finish();
        }/**/
    }
}
