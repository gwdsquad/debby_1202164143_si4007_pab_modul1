/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.petpediaaaa;

/**
 * Data model for each row of the RecyclerView
 */
class Home {

    // Member variables representing the title and information .
    private String title,isi,foto;
    private final int imageResource;

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Home(String title, String imageResource, String isi) {
        this.foto = imageResource;
        this.title = title;
        this.isi = isi;
        switch (imageResource){
            case "anjing" :
                this.imageResource = R.drawable.anjing;
                break;
            case "kucing" :
                this.imageResource = R.drawable.kucing;
                break;

            case "kelinci" :
                this.imageResource = R.drawable.kucing;
                break;

            default:
                this.imageResource = R.drawable.kucing;
                break;

        }
//        this.imageResource = imageResource;
    }

    public Home(String title, String isi) {
        this.title = title;
        this.isi = isi;
        this.imageResource = R.drawable.anjing;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }


    public Home(String title, int imageResource) {
        this.title = title;
        this.imageResource = imageResource;
    }


    String getTitle() {
        return title;
    }

    public int getImageResource() {
        return imageResource;
    }

}