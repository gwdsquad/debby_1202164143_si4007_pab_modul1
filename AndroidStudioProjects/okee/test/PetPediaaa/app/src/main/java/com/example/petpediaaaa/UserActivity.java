package com.example.petpediaaaa;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.example.petpediaaaa.Adapter.AdapterFragment;
import com.google.firebase.auth.FirebaseAuth;

public class UserActivity extends AppCompatActivity {

    ViewPager viewPager;
    BottomNavigationView bottomNavigationView;
    AdapterFragment adapterFragment;
    FirebaseAuth mAuth;
    SharedPreferences sharedPreferences;
    HomeFragment homeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        aturTema(sharedPreferences.getBoolean("darkMode",false));
        setContentView(R.layout.activity_user);

        mAuth = FirebaseAuth.getInstance();
        homeFragment = new HomeFragment();

        viewPager = findViewById(R.id.viewPager);
        bottomNavigationView = findViewById(R.id.navigation);
        adapterFragment = new AdapterFragment(getSupportFragmentManager());
        adapterFragment.addFragment(homeFragment);
        adapterFragment.addFragment(new HotelHewan());
        adapterFragment.addFragment(new ProfileFragment());
        adapterFragment.addFragment(new SettingFragment());
        viewPager.setAdapter(adapterFragment);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                bottomNavigationView.getMenu().getItem(i).setChecked(true);
                if (i==2){
                    if (mAuth.getCurrentUser()==null){
                        startActivity(new Intent(UserActivity.this,SignInActivity.class));
                    }else {
//                            ProfileFragment.name.setText(mAuth.getCurrentUser().getUid());
                        viewPager.setCurrentItem(2);

                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.navigation_home:
                        homeFragment = new HomeFragment();
                        viewPager.setCurrentItem(0);
                        homeFragment.init();
                        break;
                    case R.id.navigation_hotel:
                        viewPager.setCurrentItem(1);
                        break;
                    case R.id.navigation_akun:
                        if (mAuth.getCurrentUser()==null){
                            startActivity(new Intent(UserActivity.this,SignInActivity.class));
                        }else {
//                            ProfileFragment.name.setText(mAuth.getCurrentUser().getUid());
                            viewPager.setCurrentItem(2);

                        }
                        break;
                    case R.id.navigation_setting:
                        viewPager.setCurrentItem(3);
                        break;

                }
                return true;
            }
        });
    }

    void aturTema(boolean dark){
        if (!dark){
            setTheme(R.style.lightBar);
        }else{
            setTheme(R.style.darkBar);
        }
    }
}
